var form;
var objValidator;
var baseUrl = document.getElementsByTagName('base')[0].href;

//region General
$(document).ready(function () {
    $.validator.addMethod( //override email with django email validator regex - fringe cases: "user@admin.state.in..us" or "name@website.a"
        'email',
        function(value, element){
            return this.optional(element) || /(^([-!#$%&'*+/=?^_`{}|~0-9A-Z]{2,})+(\.([-!#$%&'*+/=?^_`{}|~0-9A-Z]{2,})+)*|^"(([\001-\010\013\014\016-\037!#-\[\]-\177]{2,})|\\([\001-\011\013\014\016-\177]{2,}))*")@((?:([A-Z0-9]{2,})(?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)$)|\[(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}\]$/i.test(value);
        },
        'Verify you have a valid email address.'
    );
});
function validaEntero(txt) {//Valida para una caja de texto que solamente se digiten números
    if (isNaN(String.fromCharCode(event.keyCode)) && !(String.fromCharCode(event.keyCode) == "-" && txt.value.indexOf("-") < 0))
        return false;
    if (String.fromCharCode(event.keyCode) == "-") {
        txt.value = "-" + txt.value;
        return false;
    }
    return true;
}
$('.form .mobile').click(function(){if ($(this).hasClass('activo')) {
	$(this).removeClass('activo');$('.cotizador').removeClass('activo');$('.body').removeClass('cotizActivo');}
	else{$(this).addClass('activo');$('.cotizador').addClass('activo');$('.body').addClass('cotizActivo');}});
$('.cerrarMenu').click(function(){if ($(this).hasClass('activo')) {
	$(this).removeClass('activo');$('.nav').removeClass('activo');$('.body').removeClass('menuActivo');}
	else{$(this).addClass('activo');$('.nav').addClass('activo');$('.body').addClass('menuActivo');}});
//endregion
//region Loading
function InicioCarando() {
    $('body').loading({
        message: '<svg width="80px"  height="80px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-ring" style="background: none;">\n' +
            '    <defs>\n' +
            '      <mask ng-attr-id="{{config.cpid}}" id="lds-flat-ring-a1b5111eb6e61">\n' +
            '        <circle cx="50" cy="50" r="45" ng-attr-fill="{{config.base}}" fill="#e0e0e0"></circle>\n' +
            '      </mask>\n' +
            '    </defs>\n' +
            '    <circle cx="50" cy="50" ng-attr-r="{{config.radius}}" ng-attr-stroke="{{config.stroke2}}" ng-attr-stroke-width="{{config.width}}" fill="none" r="28" stroke="#236EDD" stroke-width="10"></circle>\n' +
            '    <circle cx="50" cy="50" ng-attr-r="{{config.radius}}" ng-attr-stroke="{{config.stroke}}" ng-attr-stroke-width="{{config.innerWidth}}" ng-attr-stroke-linecap="{{config.linecap}}" fill="none" r="28" stroke="#4CF0FE" stroke-width="10" stroke-linecap="round" transform="rotate(6 50 50)">\n' +
            '      <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;180 50 50;720 50 50" keyTimes="0;0.5;1" dur="2s" begin="0s" repeatCount="indefinite"></animateTransform>\n' +
            '      <animate attributeName="stroke-dasharray" calcMode="linear" values="17.59291886010284 158.33626974092556;87.96459430051421 87.96459430051421;17.59291886010284 158.33626974092556" keyTimes="0;0.5;1" dur="2s" begin="0s" repeatCount="indefinite"></animate>\n' +
            '    </circle>\n' +
            '  </svg>',
        stoppable: false
    });
}

function InicioCarandoElemento(obj) {
    $('#' + obj + '').loading({
        message: '<div class="fa-1x" style="display: flex;justify-content: center;align-items: center;"><i style="width: 25px;height: 25px;font-size: 25px;" class="fas fa-cog fa-spin"></i><span style="margin-left: 7px;">Procesando</span></div>',
        stoppable: false,
        theme: 'dark'
    });
}

function FinCarando() {
    $('body').loading('stop');
}

function FinCarandoElemento(obj) {
    $('#' + obj + '').loading('stop');
}
//endregion
//region Alertas
function alertaError(msg) {
    $.alert({
        title: 'Error!',
        icon: 'fa fa-exclamation-circle ',
        content: msg,
        type: 'red',
        typeAnimated: true,
        useBootstrap: false,
        boxWidth: '20%'
    });
}

function alertSucces(msg) {
    $.alert({
        title: 'Confirmación!',
        icon: 'fa fa-check',
        content: msg,
        type: 'green',
        typeAnimated: true,
        useBootstrap: false,
        boxWidth: '20%'
    });
}
//endregion

function validaCotizForm() {
    InicioCarando();
    form = $('#cotiz_form');
    objValidator = form.validate();
    if (!form.valid()) {
        FinCarando();
    } else {
        var url = baseUrl + 'General/envioSolicitudCotizacion';
        var data = form.serialize();
        $.post({
            url: url,
            data: data,
            success: function (resp) {
                if (resp.error) {
                    FinCarando();
                    alertaError(resp.msg);
                } else {
                    $.each(form[0].elements, function (index, item) {
                        item.value = '';
                    });
                    FinCarando();
                    alertSucces(resp.msg);
                    gtag('event', 'SEND', {
                        'event_category': 'MAILS',
                        'event_label': 'COTIZACION',
                        'value': 1
                    });
                    $(document).append('<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1593114&conversionId=1710753&fmt=gif" />');

                }
            },
            error: function (resp, textStatus) {
                FinCarando();
                alertaError(resp.responseText);
            }
        });

    }
}


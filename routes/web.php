<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'General\GeneralController@index')->name('inicio');

Route::post('General/envioSolicitudCotizacion', 'General\GeneralController@envioSolicitudCotizacion')->name('general.envioSolicitudCotizacion');

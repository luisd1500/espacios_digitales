<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use App\Mail\SolicitudCotizacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class GeneralController extends Controller
{
    public function index()
    {
        $title_page = 'Inicio';
        return view('General.index', compact('title_page'));
    }

    public function envioSolicitudCotizacion(Request $request)
    {
        $error = false;
        $msg = '';
        try {
            if(env('EMAIL_TO')==null){
                $msg = 'Error: Debe Configurar EMAIL_TO en el archivo de Configuracion!';
                $error = true;
            }else{
                $objMail = new \stdClass();
                $objMail->sender_name = $request->only('nombre')['nombre'] . ' ' . $request->only('apellido')['apellido'];
                $objMail->sender_mail = $request->only('email')['email'];
                $objMail->sender_phone = $request->only('telf')['telf'];
                $objMail->sender_ciudadPais = $request->only('ciu')['ciu'];
                $objMail->sender_nroCasilleros = $request->only('ncasillero')['ncasillero'];
                $objMail->sender_cmo_ad_lock = $request->only('cmo_ad_lock')['cmo_ad_lock'];
                $objMail->subject = 'Solicitud Cotizacion de ' . $request->only('nombre')['nombre'] . ' ' . $request->only('apellido')['apellido'];
                Mail::to(env('EMAIL_TO'))->send(new SolicitudCotizacion($objMail));
                $msg = 'Solicitud enviada correctamente! Pronto sera contactado por uno de nuestros asesores';
            }

        } catch (\Exception $e) {
            $msg = 'Error: ' . $e->getMessage();
            $error = true;
        }
        return response()->json([
            'msg' => $msg,
            'error' => $error
        ]);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SolicitudCotizacion extends Mailable
{
    use Queueable, SerializesModels;
    public $objFormSolicitud = null;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( \stdClass $objFormSolicitud)
    {
        $this->objFormSolicitud=$objFormSolicitud;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(($this->objFormSolicitud->sender_mail))
            ->subject($this->objFormSolicitud->subject)
            ->view('Mails.SolicitudCotizacion')
            ->text('Mails.SolicitudCotizacion_plano');
    }
}

<!DOCTYPE html>
<html>
<head>
    <title>{!! $title_page !!} :: Espacios Digitales</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta charset="utf-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap&subset=latin-ext"
          rel="stylesheet">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <base href="{!! URL::to('/').'/' !!}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <script>     (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 1609498, hjsv: 6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv='); </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-105883407-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-105883407-1');
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '597137121053659');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=597137121053659&ev=PageView
&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    @yield('headers')
</head>
<body class="body">
<header class="header" id="header">
    <div class="menu2 dark">
        <div class="ancla">
            <div class="contLogo">
                <img src="{{asset('img/logo-espacios-digitales.png')}}">
            </div>
            <nav class="nav">
                <div class="listMenu fatherList">
                    <a href="#section_1" class="item">Casos de Uso<span class="iconMenu">›</span></a>

                    <a href="#section_2" class="item">Características<span class="iconMenu">›</span></a>

                    <a href="#section_3" class="item">Nuestro Diferencial<span class="iconMenu">›</span></a>

                    <a href="#section_4" class="item">Casos de Éxito<span class="iconMenu">›</span></a>
                </div>
            </nav>
            <div class="contIconWp"><img src="{{asset('img/iconWhatsapp.png')}}"></div>
            <div class="cerrarMenu"><span class="iconMenu"></span></div>
        </div>
    </div>
</header>
<div class="contGeneral" id="contGeneral">
    @yield('content')
</div>
<footer class="footer">
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/util.js')}}"></script>
    @yield('scripts')
</footer>

</body>
</html>

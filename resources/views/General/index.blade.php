@extends('_layouts/_layoutGeneral')

@section('description')

@endsection

@section('keywords')

@endsection

@section('headers')

@endsection

@section('content')
    <section class="sectionGeneral dark noTransparent banner">
        <div class="contImg fadeIn"><img src="{{asset('img/banner.png')}}"></div>
        <div class="spaceTop"></div>
        <h1 class="title">Smart lockers</h1>
        <p class="p1">Comienza ahora a incrementar tu productividad y a disminuir el coste de tus operaciones.</p>
        <p class="p1">Tenemos para ti la solución que te hará automatizar la logística de tus entregas, distribución o
            dispensación de productos mediante nuestros Smart Lockers.</p>
        <a href="" class="btnGeneral fadeIn">Conocer más</a>
        <div class="contClient fadeInTop">
            <div
                style="width: 190px; display: inline-flex; padding:0 25px 0 25px; vertical-align: middle; height: 100%;align-items: center;">
                <h3 style="font-size: 20px; color: #858686;font-weight: 600;">
                    Ellos confiaron en nosotros
                </h3>
            </div>
            <div style="width: calc(100% - 240px); display: inline-block; vertical-align: middle;">
                <picture>
                    <source class="rectangleLogo" srcset="{{asset('img/rectangleLogo2.png')}}"
                            media="(max-width: 930px)">
                    <source class="rectangleLogo" srcset="{{asset('img/rectangleLogo.png')}}">
                    <img class="rectangleLogo" src="{{asset('img/rectangleLogo.png')}}"
                         style="width: auto; max-height: 100%;filter: grayscale(1);opacity: .5;">
                </picture>
            </div>
        </div>
    </section>
    <section class="sectionGeneral light pasoButtom noTransparent fadeIn casosDeUso">
        <h3 style="text-align: center;">Casos de uso</h3>
        <div class="row w100" style="margin-top: 30px;">
            <div class="column W33" style="text-align: center;">
                <img class="icon" src="{{asset('img/icon001.png')}}">
                <p class="p1">Logística automatizada para entrega y dispensación de productos</p></div>
            <div class="column W33" style="text-align: center;">

                <img class="icon" src="{{asset('img/icon002.png')}}">
                <p class="p1">Uso en oficinas, exteriores y comercial</p></div>
            <div class="column W33" style="text-align: center;">

                <img class="icon" src="{{asset('img/icon003.png')}}">
                <p class="p1">Ideal para: Retail, logística de insumos para oficina, elementos
                    de seguridad, instituciones educativas</p></div>
        </div>
    </section>
    <section class="sectionGeneral light noTransparent">
        <h3 class="fadeIn">Características</h3>
        <div class="row W100 caracteristicas fadeIn">
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon004.png')}}">
                </div>
                <p class="p1">Usos: Entregas y devoluciones de mercancía.</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon005.png')}}">
                </div>
                <p class="p1">Plataforma de administración de casilleros.</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon006.png')}}">
                </div>
                <p class="p1">Integración con e-commerce o plataforma del cliente mediante API.</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon007.png')}}">
                </div>
                <p class="p1">Ahorro de personal.</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon008.png')}}">
                </div>
                <p class="p1">Trazabilidad de entregas.</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon009.png')}}">
                </div>
                <p class="p1">Automatización de procesos logísticos (internos y externos).</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon010.png')}}">
                </div>
                <p class="p1">Reemplazo de filas para mejorar la experiencia de servicio al cliente.</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon011.png')}}">
                </div>
                <p class="p1">Inteligentes & conectadas a internet.</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon012.png')}}">
                </div>
                <p class="p1">Abastecimiento y retiro por medio de QR.</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon013.png')}}">
                </div>
                <p class="p1">Notificaciones mediante SMS o Mail.</p>
            </div>

            <div class="contCampo">
                <a href="#section_4" class="btnGeneral otro">Ver galería de fotos</a>
            </div>


        </div>
    </section>

    <section class="sectionGeneral dark noTransparent" style="background: #2C3999; overflow: hidden;">
        <div class="contImg"><img src="{{asset('img/fondoSectionAzul.png')}}"></div>
        <h2 class="tituloGrande fadeIn">Nuestro diferencial</h2>

        <div class="row W100 caracteristicas diferencial fadeInTop">
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon014.png')}}">
                </div>
                <p class="p1">Personalización</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon015.png')}}">
                </div>
                <p class="p1">Mejor tiempo de compra</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon016.png')}}">
                </div>
                <p class="p1">Soporte en línea</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon017.png')}}">
                </div>
                <p class="p1">Bajo precio</p>
            </div>
            <div class="column W50">
                <div class="contIconCatacte">
                    <img src="{{asset('img/icon018.png')}}">
                </div>
                <p class="p1">Api disponible, para conectar a cualquier software</p>
            </div>
        </div>
    </section>

    <section class="sectionGeneral light noTransparent">
        <h3 class="fadeIn">Galería de fotos</h3>
        <div class="galeria">
            <div class="contImgUnica W55 fadeInTop">
                <img src="{{asset('img/imgGaleria001.png')}}">
            </div>
            <div class="contImgDoble W45 fadeInTop">
                <img src="{{asset('img/imgGaleria002.png')}}">
                <img src="{{asset('img/imgGaleria003.png')}}">
            </div>
            <div class="contImgDoble W45 fadeInTop">
                <img src="{{asset('img/imgGaleria004.png')}}">
                <img src="{{asset('img/imgGaleria005.png')}}">
            </div>
            <div class="contImgUnica W55 fadeInTop">
                <img src="{{asset('img/imgGaleria006.png')}}">
            </div>
        </div>
    </section>


    <section class="cotizador dark noTransparent">

        <div class="contImg fixed"><img src="{{asset('img/fondoForm.png')}}"></div>
        <h2>Cotiza <b>tu casillero</b></h2>
        <form class="form" id="cotiz_form" novalidate="novalidate">
            {{csrf_field()}}
            <div class="contCampo">
                <label>Nombre</label>
                <input type="text" class="campo" name="nombre" data-rule-required="true" data-msg-required="Campo Requerido">
            </div>
            <div class="contCampo">
                <label>Apellido</label>
                <input type="text" class="campo" name="apellido" data-rule-required="true" data-msg-required="Campo Requerido">
            </div>
            <div class="contCampo">
                <label>Teléfono de contacto</label>
                <input type="tel" class="campo" name="telf" data-rule-required="true" data-msg-required="Campo Requerido" onkeypress="return validaEntero(this.value);">
            </div>
            <div class="contCampo">
                <label>Email</label>
                <input type="email" class="campo" name="email" data-rule-required="true" data-rule-email="true"
                       data-msg-required="Campo Requerido" data-msg-email="Formato incorrecto">
            </div>
            <div class="contCampo">
                <label>Ciudad</label>
                <input type="text" class="campo" name="ciu" placeholder="" data-rule-required="true"
                       data-msg-required="Campo Requerido">
            </div>
            <div class="contCampo">
                <label>Número de casilleros</label>
                <select name="ncasillero" class="campo" data-rule-required="true" data-msg-required="Campo Requerido">
                    <option value="">Seleccione</option>
                    <option value="1">1</option>
                </select>
            </div>
            <div class="contCampo customRadio errorCustomTop">
                <label>¿Cómo desea adquirir los lockers?</label>
                <div class="contCampo W50" style="margin-right: 15px;">
                    <label for="radio1">Alquilar</label>
                    <input class="campo" type="radio" name="cmo_ad_lock" id="radio1" value="Alquilar" data-rule-required="true"
                           data-msg-required="*">
                    <label class="campoRadio" for="radio1"></label>
                </div>
                <div class="contCampo W50">
                    <label for="radio2">Comprar</label>
                    <input class="campo" type="radio" name="cmo_ad_lock" id="radio2" value="Comprar">
                    <label class="campoRadio" for="radio2"></label>
                </div>
            </div>
            <div class="contCampo custom errorCustomTop">
                <label>Por favor lea nuestras <a href="https://espaciosdigitales.co/wp-content/uploads/2019/08/Proteccion-de-datos-V2.pdf">Politicas de Privacidad</a></label>
                <input type="checkbox" name="terminosYcondiciones" data-rule-required="true" data-msg-required="*" /><label style="display: inline-block;width: calc(100% - 30px);left: 10px;">Estoy de acuerdo con las politicas de privacidad</label>
            </div>
            <div class="contCampo">
                <a onclick="validaCotizForm()" class="btnGeneral">COTIZAR</a>
            </div>

            <div class="contCampo mobile">
                <a class="btnGeneral"></a>
            </div>
        </form>
    </section>
@endsection

@section('scripts')

@endsection


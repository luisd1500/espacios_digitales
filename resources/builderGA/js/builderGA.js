// variables
    var content = $('#contGeneral');
    var secciones = content.children();
    var nav = $('#header').offset();
    var navTop = nav.top;
    var logo = $('#contLogo');
    var video = $('.autoplayVideo');
    var altoDePantalla=$(window).height();
    var anchoDePantalla=$(window).width();
    var anchoMenu = $('.fatherList').width()
    var parrafos = $('p');
//Cambia el color del menú y el logo según el fondo
function funcionCamaleonica() {
    $(secciones).each(function () {
        var _thisOffset = $(this).offset();
        var _actPosition = _thisOffset.top - $(window).scrollTop();

        if (_actPosition < nav.top && _actPosition + $(this).height() >= 60) {
            $("#header").removeClass("light dark").addClass($(this).hasClass("light") ? "light" : "dark").css('background-color',$(this).css('background-color'));
            $("#header nav ul li ul").removeClass("light dark").addClass($(this).hasClass("light") ? "light" : "dark").css('background-color',$(this).css('background-color'));
            $(".pagination").removeClass("light dark").addClass($(this).hasClass("light") ? "light" : "dark");
            if(nav.top>0&&nav.top<=1){
                $("#header").removeClass("transparent noTransparent").addClass($(this).hasClass("transparent") ? "transparent" : "noTransparent").css('background-color',$(this).css('background-color'));
                $("#header nav ul li ul").removeClass("transparent noTransparent").addClass($(this).hasClass("transparent") ? "transparent" : "noTransparent").css('background-color',$(this).css('background-color'));
            }
            else{
                $("#header").removeClass("transparent noTransparent");
                $("#header nav ul li ul").removeClass("transparent noTransparent");
            }

            $("#nav").removeClass("light dark").addClass($(this).hasClass("light") ? "light" : "dark");
            $("#contLogo").removeClass("light dark").addClass($(this).hasClass("light") ? "light" : "dark");
            return false;
        }
    });
}
$('.desplegable')
    .mouseover(function() {
        $('#header').addClass('subActive');
    })
    .mouseout(function() {
        $('#header').removeClass('subActive');
    });

function stikyHeader() {
    if ($('#header').height() > 100) {
        $('#header').addClass('transparent');
    } else {
        $('#header').removeClass('transparent');
    }
}


$("a").on('click', function(event) {
    if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top - 0
        }, 1200, "swing",  function(){
            window.location.hash = hash;
        });
    }
});



$('.abrirPopUp').click(function  (event) {
    event.preventDefault();
    $('.popUp').addClass('activo');
    $('.pp').attr('style','display:none;');
    $('body').addClass('popUpActivo');
});



$('.iconCerrarPopUp').click(function() {
    $('.popUp').removeClass('activo');
    $('body').removeClass('popUpActivo');
});


$('.btnWizard').click(function () {
    if ($(this).hasClass('activo') == false) {
        $('.btnWizard').removeClass('activo');
        $(this).addClass('activo');
    }
});


function p1(){
    $(parrafos).each(function(){
        if($(this).hasClass('p1')==false){
            $(this).addClass('p1')
        }
    })
};

//Activa y desactiva los videos cuando son vicibles
function activarVideo(){
    $(video).each(function(){
            var _thisOffsetVideo = $(this).offset();
            var _actPositionVideo = _thisOffsetVideo.top - $(window).scrollTop();
        if (_actPositionVideo < navTop && _actPositionVideo + $(this).height() > 100) {
            if (anchoDePantalla < 600) {
                if($(video).hasClass('videoMobile')){
                    $(this).trigger('play');
                }else {
                    $(this).trigger('pause');
                }
            }else{
                if($(video).hasClass('videoMobile')){
                    $(this).trigger('play');
                }else {
                    $(this).trigger('pause');
                }
            }
        }else{
            $(this).trigger('pause');
        }
    });
}


//Animaciones
function animation() {
    $('.fadeIn').each(function (i) {
        var this_offset = $(this).offset();
        var bottom_of_object = this_offset.top + 10;
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        if (bottom_of_window > bottom_of_object) {
            $(this).animate({'opacity': '1'}, 600);
        }
    });

    $('.fadeInTop').each(function (i) {
        var this_offsetTop = $(this).offset();
        var bottom_of_objectTop = this_offsetTop.top + 10;
        var bottom_of_windowTop = $(window).scrollTop() + $(window).height();

        if (bottom_of_windowTop > bottom_of_objectTop) {
            $(this).animate({'opacity': '1', 'top': '0px'}, 600);
            //$(this).animate({'top':'0px'},1000);
        }
    });
}



//responsive
function responsive(){
    if (anchoDePantalla < 600) {
        $('#nav').removeClass('tablet');
        $('#nav').addClass('mobile');
        $('.paddingWidth').addClass('noPaddingWidth');
        $('.paddingWidth').removeClass('paddingWidth');
    }else{
        $('#nav').removeClass('mobile');
        $('#nav').addClass('tablet');
        $('.noPaddingWidth').addClass('paddingWidth');
        $('.noPaddingWidth').removeClass('noPaddingWidth');
    };
}


//cerrar menu en mobile touch
function cerrarMenuTouch() {
    $('.fatherList').each(function (i) {
        var this_offsetLeft = $(this).offset();
        var left_of_objectLeft = this_offsetLeft.left;
        var left_of_windowLeft = $(this).scrollLeft();

        if (left_of_windowLeft > (left_of_objectLeft)) {
            $('.btnMenu').removeClass('activo');
            $('#header').removeClass('menuActivo');
            $('#nav').removeClass('menuActivo');
            $('body').removeClass('offScroll');
        }
    });
}


$('.fatherList').scroll(function(){
    cerrarMenuTouch();
});




$( document ).ready(function() {
    funcionCamaleonica();
    activarVideo();
    animation();
    responsive();
    stikyHeader();
    cerrarMenuTouch();
    p1();
    for (var i = 0; i < secciones.length; i++ ){
        $(secciones[i]).attr('id', 'section_' + i);
    }
});

$(window).scroll(function(){
    nav = $('.fatherList').offset();
    navTop = nav.top;
    funcionCamaleonica();
    activarVideo();
    animation();
    stikyHeader();
});

$(window).resize(function(){
    altoDePantalla=$(window).height();
    anchoDePantalla=$(window).width();
    responsive();
});


$('.btnMenu').click( function(){

    if ($(this).hasClass('activo')) {
        $(this).removeClass('activo');
        $('#header').removeClass('menuActivo');
        $('#nav').removeClass('menuActivo');
        $('body').removeClass('offScroll');
    }else{
        $(this).addClass('activo');
        $('#header').addClass('menuActivo');
        $('#nav').addClass('menuActivo');
        $('body').addClass('offScroll');
    }
});

